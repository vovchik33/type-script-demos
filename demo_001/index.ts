interface IPerson {
    job:string,
    firstName:string,
    lastName:string,
    age:number
}

const Stew = {
    job:"Worker",
    firstName:"Stew",
    lastName:"Stew",
    age: 100
}

class Person {
    private person:IPerson;

    constructor(person:IPerson) {
        this.person = person;
    }

    public sayHello() {
        console.log(`Hello, ${this.person.firstName}!`);
    }
}
const stew:Person = new Person(Stew);
stew.sayHello();