var Stew = {
    job: "Worker",
    firstName: "Stew",
    lastName: "Stew",
    age: 100
};
var Person = (function () {
    function Person(person) {
        this.person = person;
    }
    Person.prototype.sayHello = function () {
        console.log("Hello, " + this.person.firstName + "!");
    };
    return Person;
})();
var stew = new Person(Stew);
stew.sayHello();
