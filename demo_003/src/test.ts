interface IPerson {
    // firstName: string;
    // secondName: string;

    getFullName();
}

class Student implements IPerson {
    firstName: string;
    secondName: string;
    constructor(firstName: string, secondName: string) {
        this.firstName = firstName;
        this.secondName = secondName;
    }
    getFullName() {
        return `${this.firstName} ${this.secondName}`;
    }
}
function greetPerson(person: IPerson) {
    console.log("Hello, " + person.getFullName());
}

let student = new Student("Ivan", "Ivanych");
let studentKa = new Student("Lena", "Petrovna");
greetPerson(student);
greetPerson(studentKa);
