var Student = /** @class */ (function () {
    function Student(firstName, secondName) {
        this.firstName = firstName;
        this.secondName = secondName;
    }
    Student.prototype.getFullName = function () {
        return this.firstName + " " + this.secondName;
    };
    return Student;
}());
function greetPerson(person) {
    console.log("Hello, " + person.getFullName());
}
var student = new Student("Ivan", "Ivanych");
var studentKa = new Student("Lena", "Petrovna");
greetPerson(student);
greetPerson(studentKa);
//# sourceMappingURL=test.js.map